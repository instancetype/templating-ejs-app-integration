/**
 * Created by instancetype on 6/20/14.
 */
var ejs = require('ejs')
  , fs = require('fs')
  , http = require('http')
  , filename = './views/sirens.ejs'

var sirens = [ { name: 'Harleen Quinzel'
               , age: 27
               }

             , { name: 'Pamela Isley'
               , age: 26
               }

             , { name: 'Selina Kyle'
               , age: 26
               }
             ]

var server = http.createServer(function(req, res) {
  if (req.url === '/') {
    fs.readFile(filename, function(err, data) {
      var template = data.toString()
        , context = {sirens: sirens}
        , cache = process.env.NODE_ENV === 'production'

        , output = ejs.render( template
                             , { sirens: sirens
                               , cache: cache
                               , filename: filename
                               }
                             )

      res.setHeader('Content-Type', 'text/html')
      res.end(output)
    })
  } else {
    res.statusCode = 404
    res.end('Not Found')
  }
}).listen(3000)